//
//  main.c
//  employ information system
//
//  Created by Hong Yao on 2020/11/2.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hong.h"

//the main function
int main()
{
    typedef int status;

    FILE *fp;  //define the file type
	char file_name[50];
	printf("Please input file name¡ª¡ª:\n");
	scanf("%s",file_name);
	if (open_file(file_name)==-1){
		printf("Can not open the file\n");
		return -1;
	}
	fp=fopen(file_name,"r");


	int number=0;
	int workerList[1024];
	struct worker workers[1024];
	//
	while(!feof(fp)){
		fscanf(fp,"%d  %s  %s  %d\n",&workers[number].ID_number,&workers[number].first_name,&workers[number].last_name,&workers[number].salary);
		workerList[number]=workers[number].ID_number;
		number++;
	}
	fclose(fp);


    int flag;

    int six_digit_id;//define the variable, employee ID No.
    char last_name[64];
    char first_name[64];
    int salary;
    int yes_or_no;


    while(1)
     {
            printf("\tEmployee Information System\n");
            printf("==================Menu==================\n ");
            printf("1.Print the Table of Employee Information\n ");
            printf("2.Look up by Employee's ID number\n ");
            printf("3.Look up by Employee's Last Name\n ");
            printf("4.Add Employee Information\n ");
            printf("5.Quit\n ");

            printf("Please enter your option£º");
            scanf("%d", &flag);

            //sort the employee information by ID No. in increasing order
            qsort(workers,number,sizeof(workers[0]),comp);
			for(int i=0;i<number;i++){
		 	   workerList[i]=workers[i].ID_number;
			}
            switch (flag)
            {

                case 1:
                    Display(workers,number);
                    break;            // Print the tale of employee information
                case 2:
                    Bisearch(workers,workerList,0,number,six_digit_id);
                    break;                   // Lookup employee information by ID No.
                case 3:
                    //search_lastname(workers,number);
                    Look_up_by_Lastname(workers,number,last_name);
                    break;             //Lookup employee information by last name
                case 4:
                   // Add(workers,first_name,last_name,salary,number,workerList);
                   // Save(worker);

                    printf("Enter the first name of the employee: ");
                    read_string(first_name);  //apply the read_string
                    printf("Enter the last name of the employee: ");
                    read_string(last_name);
                    while(1){
                        printf("Enter employee's salary ($30,000 and $150,000): ");
                        read_int(&salary);
                        printf("Do you really want to add the information into the database?\n");
                        printf("%s %s , salary: %d\n",first_name,last_name,salary);
                        printf("whether saved to a file£¿( 1-----yes£¡£¬0-----no£¡ )");
                        read_int(&yes_or_no);
                            if(yes_or_no==1){
                            if(salary>=30000&&salary<=150000){
                                int cur_id=workers[number-1].ID_number;
                                int new_id=cur_id+1;
                                if(new_id<100000||new_id>999999){
                                    printf("id number is out of bound\n");
                                    break;
                                }else{
                                    strcpy(workers[number].first_name,first_name);
                                    strcpy(workers[number].last_name,last_name);
                                    workers[number].salary=salary;
                                    workers[number].ID_number=new_id;
                                    workerList[number]=workers[number].ID_number;
                                    number++;
                                    printf("adding successfully.\n");
                                    break;
                                }

                            }else{
                                printf("salary invalid, please enter again.\n");
                            }
                        }
                    }

                    break;           //add employee information and save to the file
                case 5:
                    printf(" \nPrompt: Do u really want to quit the system? \n ");
                    exit(0);
                    break;   //Quit the system
                default:
                    printf("Prompt: wrong input!\n ");
            }
    }
}

//   D://Desktop/input.txt
