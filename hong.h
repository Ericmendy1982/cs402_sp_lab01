#define N 1024
#define MAX 64


//whether to open the file
int open_file(char *file_name){
    if(fopen(file_name, "r") == NULL){
		return -1;
	}
    return 0;
}

struct worker
{
    int ID_number;       //employee  ID_No. (6-digits,100000-999999)
    char first_name[MAX];       //first name, the length can not be more than 64
    char last_name[MAX];       //last name
    int salary;          //salary
};

int read_int(int *variable){
    if(scanf("%d", variable) == 0){
		return -1;
	}
    return 0;
}

int read_float(float *variable){
    if(scanf("%f", variable) == 0){
		return -1;
	}
    return 0;
}

int read_string(char *variable){
    if(scanf("%s", variable) == 0){
		return -1;
	}

    return 0;
}

void close_file(FILE *file_name){
	fclose(file_name);
}

/*
compare specified variables,
it would be used in sort a list.
*/
int comp(const void *p1,const void *p2){
	int id1=((struct worker *)p1)->ID_number;
	int id2=((struct worker *)p2)->ID_number;
	return id1>id2?1:-1;
}



//Add employee information in increasing order of ID No.
void Add(struct worker workers[N],char first_name[MAX],char last_name[MAX],int salary,int number,int list[N])
{
    int yes_or_no;
    printf("Enter the first name of the employee: ");
    read_string(first_name);  //调用read_string方法
    printf("Enter the last name of the employee: ");
    read_string(last_name);
    printf("Enter employee's salary ($30,000 and $150,000): ");
    read_int(&salary);
    printf("你打算把下列信息加入到数据库表中吗?\n");
    printf("%s %s , salary: %d\n",first_name,last_name,salary);
    printf("whether saved to a file？( 1-----yes！，0-----no！ )");
    read_int(&yes_or_no);
    if(yes_or_no==1){
        if(salary>=30000&&salary<=150000){
            int cur_id=workers[number-1].ID_number;
            int new_id=cur_id+1;
            if(new_id<100000||new_id>999999){
                printf("id number is out of bound\n");
            }else{
                struct worker temp;

                strcpy(temp.first_name,first_name);
                strcpy(temp.last_name,last_name);
                temp.ID_number = new_id;
                temp.salary = salary;
               // strcpy(workers[number].first_name,first_name);
                //strcpy(workers[number].last_name,last_name);
               // workers[number].salary=salary;
                //workers[number].ID_number=new_id;
                workers[number] = temp;
               list[number]=workers[number].ID_number;

                number++;
                printf("adding successfully.\n");
            }

        }else{
            printf("salary invalid, please enter again.\n");
        }
    }

}


//binary search by ID No.
int binary_search(const int arr[],int min,int max,int target){
	if(max>=min){
		int mid=min+(max-min)/2;
		if(arr[mid]==target){
			return mid;
		}else if(arr[mid]>target){
			return binary_search(arr,min,mid-1,target);
		}else{
			return binary_search(arr,mid+1,max,target);
		}
	}
	return -1;
}

//Lookup function, by employee's ID No.
void Bisearch(struct worker workers[N],const int arr[],int min,int max,int target){

    int worker_id;  //

    printf("Please type in the employee's ID No.:\n");
    scanf("%d",&target);
    //binary search
    if(max>=min){
		int mid=min+(max-min)/2;
		if(arr[mid]==target){
			return mid;
		}else if(arr[mid]>target){
			worker_id= binary_search(arr,min,mid-1,target);
		}else{
			worker_id= binary_search(arr,mid+1,max,target);
		}
	}else{worker_id=-1;}

    if(worker_id==-1){
        printf("Employee with id %d not found in DB\n",target);
    }else{
		printf("\n %d\t %s\t %s\t %s\t \n ", workers[worker_id].ID_number, workers[worker_id].first_name,
                workers[worker_id].last_name, workers[worker_id].salary);

    }

}

//search by last name
int search_lastname(struct worker workers[N],int number){
    char find_lastname[64];
    printf("\n please enter the employee's  last name：");
    read_string(find_lastname);
	int i=0;
	while(i!=number){
		if(strcmp(workers[i].last_name,find_lastname)==0){
			return i;
		}
		i++;
	}
	return -1;
}

//Lookup function, by employee's last name
void  Look_up_by_Lastname(struct worker workers[N],int number,char lastname[])
{
    int key;
    key=search_lastname(workers,number);
    int i=0;
    if(key==-1){
        printf("Employee with lastname %s not found in DB\n",lastname);
    }else{
      printf(" ID_number   first_name     last_name     salary   \n ");
      printf(" %-10s %-10s %10d %10d \n ", workers[key].first_name, workers[key].last_name,workers[key].ID_number, workers[key].salary);
    }
}


//print the table of employee's information
void  Display(struct worker workers[N],int number)
{
    printf("\t\t\t\t Table Of Employee's Information \n");
    printf(" \n ID_number   first_name     last_name     salary   \n ");
    int i=0;
    while (i!=number)
    {
        printf("%-10s %-10s %10d %10d \n",workers[i].first_name,workers[i].last_name,workers[i].salary,workers[i].ID_number);
				i++;

    }
    printf("++++++++++++++++++++++++++++++++++++");
    printf("Number of Employees (%d)\n",number);

}
